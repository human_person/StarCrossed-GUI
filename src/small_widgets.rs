use std::time::Instant;
use iced::{Color, Element, Length, Padding, Point, Rectangle, Renderer, Size, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced::widget::{Canvas, Text};
use iced::widget::canvas::{Cache, Cursor, Geometry, Program};
use iced_native::row;
use iced_native::widget::{button, container, text};
use crate::grid::GridMessage;
use crate::Message;

/// Widget to display game scored
#[derive(Clone)]
pub struct ScoreWidget {
	pub left: u16,
	pub right: u16,
}

impl ScoreWidget {
	/// New widget with scored of 0
	pub fn new() -> Self {
		Self { left: 0, right: 0 }
	}
	
	/// View the widget
	pub fn view(&self, height: Length) -> Element<Message> {
		let canvas = Canvas::new(self)
			.width(Length::Fill)
			.height(height);
		let padding = 20.;
		let content = row![
			text(format!("{}", self.left)).vertical_alignment(Vertical::Center).horizontal_alignment(Horizontal::Left).width(Length::Shrink).height(Length::Fill),
			canvas,
			text(format!("{}", self.right)).vertical_alignment(Vertical::Center).horizontal_alignment(Horizontal::Right).width(Length::Shrink).height(Length::Fill)
		].spacing(20).padding(Padding::from([0., padding]));
		container(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x()
            .center_y()
            .into()
	}
}

impl Program<Message> for ScoreWidget {
	type State = ();
	
	fn draw(&self, _state: &Self::State, theme: &Theme, bounds: Rectangle, _cursor: Cursor) -> Vec<Geometry> {
		let (red, blue) = match theme {
			Theme::Light => (Color::new(1., 0., 0., 0.5), Color::new(0., 0., 1., 0.5)),
			_ => (Color::from_rgba8(155, 0, 0, 1.), Color::from_rgba8(0, 0, 155, 1.))
		};
		let cache = Cache::new().draw(bounds.size(), |f| {
			if self.left == 0 && self.right == 0 {
				f.fill_rectangle(Point::new(0., 0.), Size::new(bounds.width / 2., bounds.height), red);
				f.fill_rectangle(Point::new(bounds.width / 2., 0.), Size::new(bounds.width / 2., bounds.height), blue);
			} else {
				let w = self.left as f32 / (self.left + self.right) as f32;
				f.fill_rectangle(Point::new(0., 0.), Size::new(w * bounds.width, bounds.height), red);
				f.fill_rectangle(Point::new(w * bounds.width, 0.), Size::new((1. - w) * bounds.width, bounds.height), blue);
			}
		});
		vec![cache]
	}
}

/// Move indicator
#[derive(Clone)]
pub struct MoveIndicator(pub usize);

impl MoveIndicator {
	/// View either a red or blue bar depending on whose turn it is
	pub fn view(&self, height: Length) -> Element<Message> {
		Canvas::new(self)
			.width(Length::Fill)
			.height(height)
            .into()
	}
}

impl Program<Message> for MoveIndicator {
	type State = ();
	
	fn draw(&self, _state: &Self::State, _theme: &Theme, bounds: Rectangle, _cursor: Cursor) -> Vec<Geometry> {
		let cache = Cache::new().draw(bounds.size(), |f| {
			if self.0 == 0 {
				f.fill_rectangle(Point::ORIGIN, bounds.size(), Color::new(1., 0., 0., 1.))
			} else {
				f.fill_rectangle(Point::ORIGIN, bounds.size(), Color::new(0., 0., 1., 1.))
			}
		});
		vec![cache]
	}
}

/// Move timers
#[derive(Clone)]
pub struct MoveTimer {
	/// Total deciseconds (10s of milliseconds) of time spent playing
    pub total: u128,
	/// Start time of current turn
    start: Option<Instant>
}

impl MoveTimer {
	/// Update [`total`](MoveTimer::total) at the current time
    pub fn end(&mut self) {
        if let Some(t) = &self.start {
            self.total += t.elapsed().as_millis() / 10;
            self.start = None
        }
    }
    
	/// Start the times
    pub fn start(&mut self) {
        self.start = Some(Instant::now())
    }
    
	/// Get a new timer with no total time so far
    pub fn new() -> Self {
        Self { total: 0, start: None }
    }
    
	/// View the timer
    pub fn view(&self) -> Text {
        let total = if let Some(t) = &self.start {
            (self.total + t.elapsed().as_millis() / 10) / 100
        } else {
            self.total / 100
        };
        text(format!("{:>02}:{:>02}", total / 60, total % 60))
    }
}

/// Menu at the top of the window when playing the game
pub struct PlayerMenu;

impl<'a> PlayerMenu {
	/// View the widget
	pub fn view() -> Element<'a, Message, Renderer<Theme>> {
		row![
			button("Home").width(Length::Shrink).on_press(Message::Home),
			button("Save game").width(Length::Shrink).on_press(Message::Grid(GridMessage::SaveGameOverlay)),
			button("Export game").width(Length::Shrink).on_press(Message::Grid(GridMessage::ExportFull)),
			button("Copy state").width(Length::Shrink).on_press(Message::Grid(GridMessage::CopyState)),
		].padding([0, 0, 3, 0]).spacing(3).width(Length::Fill).height(Length::Shrink).into()
	}
}
