//! This example showcases a simple native custom widget that draws a circle.
mod grid;
mod small_widgets;
mod player;
mod menu;
mod grid_settings;

use std::path::PathBuf;
use dark_light::Mode;
use iced::{Application, Element, executor, Renderer, Settings, Theme};
use iced::window::close;
use iced_native::{Command, Subscription};
use crate::grid::GridMessage;
use crate::grid_settings::NewOptionMessage;
use crate::menu::{HomeMenu, MenuMessage};
use crate::player::Player;

pub fn main() -> iced::Result {
    let p = if let Some(p) = dirs::config_dir() {
        p.join("StarCrossed")
    } else {
        std::process::exit(1)
    };
    if !p.exists() {
        if std::fs::create_dir_all(&p).is_err() { std::process::exit(1) }
    }
    App::run(Settings {
        flags: AppConfig {
            config_dir: p
        },
        ..Default::default()
    })
}

/// # Main App
///
/// Main app controller. Contains very little logic at all and is mostly to a state controller
struct App {
    /// All config provided in the flags
    config: AppConfig,
    /// App state
    container: AppContainer,
    /// App theme
    theme: Theme
}

/// # App Config
///
/// Flags provided in [`Application`](iced::Application::Flags)
#[derive(Clone)]
pub struct AppConfig {
    pub config_dir: PathBuf
}

impl Default for AppConfig {
    fn default() -> Self {
        Self {
            config_dir: PathBuf::new()
        }
    }
}

/// # App States
///
/// All possible app states
enum AppContainer {
    /// Main menu
    Menu(HomeMenu),
    /// Game player
    Player(Player)
}

/// Minimum value for [n](crate::grid:Grid::n)
const N_MIN: usize = 1;
/// Maximum value for [n](crate::grid:Grid::n)
const N_MAX: usize = 4;

/// # Messages
///
/// Message type for [`Application`](iced::Application::Message)
#[derive(Debug, Clone, PartialEq)]
pub enum Message {
    // general messages
    /// General message to update states
    Tick,
    /// Return to the [main menu](AppContainer::Menu)
    Home,
    /// Quit the program
    Quit,
    /// Change theme to [dark](Theme::Dark) or [light](Theme::Light)
    ChangeTheme,
    
    // open a local file menu messages
    /// Open a saved file by cloning the visualised grid
    OpenFile,
    /// Select a file in the open game menu [state](crate::menu::MenuState::Open)
    SelectFile(PathBuf),
    
    // state messages
    /// Playing game specific message
    Grid(GridMessage),
    /// Menu specific messages
    Menu(MenuMessage),
    /// messages for changing new game creation
    NewOptions(NewOptionMessage)
}

impl Application for App {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = AppConfig;
    
    fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let theme = match dark_light::detect() {
            Mode::Light => Theme::Light,
            _ => Theme::Dark
        };
        let out = HomeMenu::new(flags.clone());
        (Self {
            config: flags,
            container: AppContainer::Menu(out.0),
            theme
        }, out.1)
    }
    
    fn title(&self) -> String {
        let name = "Star Crossed";
        match self.container {
            AppContainer::Menu(_) => name.to_string(),
            AppContainer::Player(ref g) => format!("{} - Playing {}×{1}", name, g.game.n * 2 + 1)
        }
    }
    
    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        let child = match &mut self.container {
            AppContainer::Menu(m) => m.update(message.clone()),
            AppContainer::Player(p) => p.update(message.clone()),
        };
        match message {
            Message::ChangeTheme => self.theme = if self.theme == Theme::Dark { Theme::Light } else { Theme::Dark },
            Message::Quit => return close(),
            Message::Home => self.container = AppContainer::Menu(HomeMenu::new(self.config.clone()).0),
            Message::OpenFile => {
                if let AppContainer::Menu(m) = &self.container {
                    let player_option = m.visualised_grid.clone();
                    let mut player = player_option.unwrap().unwrap();
                    player.game.visualiser = false;
                    self.container = AppContainer::Player(player)
                }
            }
            Message::Menu(MenuMessage::Play) => {
                if let AppContainer::Menu(m) = &self.container {
                    self.container = AppContainer::Player(Player::new((self.config.clone(), m.new_options)).0)
                }
            },
            Message::Menu(MenuMessage::PlayCustomHistory) => {
                if let AppContainer::Menu(ref m) = self.container {
                    if let Some(Ok(ref g)) = m.history_input {
                        self.container = AppContainer::Player(g.clone())
                    }
                }
            }
            Message::Menu(MenuMessage::PlayCustomState) => {
                if let AppContainer::Menu(ref m) = self.container {
                    if let Some(p) = Player::from_state(m.custom_state.clone(), self.config.clone()) {
                        self.container = AppContainer::Player(p)
                    }
                }
            }
            _ => {}
        }
        child
    }
    
    fn view(&self) -> Element<'_, Self::Message, Renderer<Self::Theme>> {
        match &self.container {
            AppContainer::Menu(m) => m.view(),
            AppContainer::Player(p) => p.view()
        }
    }
    
    fn theme(&self) -> Self::Theme {
        self.theme.clone()
    }
    
    fn subscription(&self) -> Subscription<Self::Message> {
        match &self.container {
            AppContainer::Menu(m) => m.subscription(),
            AppContainer::Player(p) => p.subscription()
        }
    }
}
