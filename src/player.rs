use std::io::Write;
use std::path::PathBuf;
use std::time::Duration;
use iced::{Alignment, Application, Color, Element, executor, Length, Renderer, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced::time::every;
use iced_aw::{Card, Modal};
use iced_native::{Command, row, column as col, Subscription};
use iced_native::widget::{button, container, text, text_input};
use crate::grid::{Grid, GridMessage, GridMove};
use crate::{AppConfig, Message, N_MAX, N_MIN};
use crate::grid_settings::GameSettings;
use crate::small_widgets::{MoveIndicator, MoveTimer, ScoreWidget, PlayerMenu};

/// # Game Player State
///
/// App state for playing a game
#[derive(Clone)]
pub struct Player {
	/// Path to the save files directory
	config: PathBuf,
	/// Scores for each grid size
    scores: Vec<ScoreWidget>,
	/// Grid to play games on
    pub game: Grid,
	/// Indicator for the current move
    move_indicator: MoveIndicator,
	/// Turn timers
    pub times: [MoveTimer; 2],
	/// Is the game being saved at the moment
	save_overlay: bool,
	/// Running value for the name the game should be saved as
	save_name: String,
	/// Error string if saving caused an error
	save_error: Option<String>,
	settings: GameSettings,
}

impl Player {
	/// Try to make `Self` from a board state (see [here](Grid::encode) for board states)
	pub fn from_state(s: String, config: AppConfig) -> Option<Self> {
		let iter = s.chars().collect::<Vec<_>>();
		let k = iter.len() - 1;
		let n = (k as f32).sqrt() as usize;
		if n * n != k || n & 1 == 0 || !(N_MIN..=N_MAX).contains(&((n - 1) / 2)) { return None }
		let mut markers = [Vec::new(), Vec::new()];
		let turn = match iter[0] {
			'r' => 0,
			'b' => 1,
			_ => return None
		};
		for i in 0..n {
			for j in 0..n {
				match iter[1 + i * n + j] {
					'r' => markers[0].push((i, j)),
					'b' => markers[1].push((i, j)),
					'_' => {},
					_ => return None
				}
			}
		}
		Some(Self {
			config: config.config_dir,
			game: Grid {
				n: (n - 1) / 2, markers, turn,
				..Default::default()
			},
			..Default::default()
		})
	}
}

impl Default for Player {
    fn default() -> Self {
        Player {
			config: PathBuf::new(),
            scores: (N_MIN..=N_MAX).map(|_| ScoreWidget::new()).collect(),
			game: Grid::default(), move_indicator: MoveIndicator(0),
            times: [MoveTimer::new(), MoveTimer::new()],
			save_overlay: false, save_name: String::new(), save_error: None,
			settings: Default::default()
        }
    }
}

impl TryFrom<(String, PathBuf)> for Player {
	type Error = ();
	
	/// Try to turn a fully saved game into a `Self` instance. See [here](Player::into) for
	/// how that works
	fn try_from((value, conf): (String, PathBuf)) -> Result<Self, Self::Error> {
		fn inner(value: String, out: &mut Grid) -> Option<[MoveTimer; 2]> {
			let mut iter = value.bytes().peekable();
			let n = iter.next()? as usize;
			let width;
			if (N_MIN..=N_MAX).contains(&n) {
				out.n = n;
				width = n * 2
			} else { return None }
            
            let mut timers = [MoveTimer::new(), MoveTimer::new()];
            
            for i in [0, 1] {
                let mut time_bytes = Vec::new();
                loop {
                    let next = iter.next()?;
                    if next == b',' { break }
					time_bytes.push(next);
                }
				if let Ok(s) = String::from_utf8(time_bytes) {
					timers[i].total = u128::from_str_radix(&*s, 10).ok()?;
				} else { return None }
            
            }
            
			let mut history = Vec::new();
			let mut markers = [Vec::new(), Vec::new()];
			let mut turn = 0;
			while iter.peek().is_some() {
				let x = iter.next()? as usize;
				if !(0..=width).contains(&x) { return None }
				let y = iter.next()? as usize;
				if !(0..=width).contains(&y) { return None }
				if markers[turn].len() < 3 * out.n {
					history.push(GridMove::Place {
						pos: (x, y),
						state: markers.clone(),
						turn
					});
					markers[turn].push((x, y));
					turn ^= 1;
					continue
				}
				let x1 = iter.next()? as usize;
				if !(0..=width).contains(&x1) { return None }
				let y1 = iter.next()? as usize;
				if !(0..=width).contains(&y1) { return None }
				let index = markers[turn].iter().position(|(i, j)| *i == x && *j == y)?;
				history.push(GridMove::Move {
					from: (x, y), to: (x1, y1),
					state: markers.clone(), turn
				});
				markers[turn][index] = (x1, y1);
				turn ^= 1;
			}
			out.markers = markers;
			out.turn = turn;
			out.history = history;
			out.history_index = out.history.len();
			Some(timers)
		}
		let mut out = Grid::default();
		if let Some(timers) = inner(value, &mut out) {
			out.turn ^= 1;
			out.check_win();
			out.turn ^= 1;
			if !out.finished {
				out.check_win()
			}
            let move_indicator = MoveIndicator(out.turn);
			Ok(Self {
                game: out,
                move_indicator,
                times: timers,
				config: conf.clone(),
                ..Default::default()
            })
		} else {
			Err(())
		}
	}
}

impl Into<String> for &mut Player {
	/// Turn a `&mut Self` into a `String`
	///
	/// 1. Writes [`n`](Game::n) as a `u8` byte (i.e. if `n = 3`, it would write the byte `3`)
	/// 2. Write the [timers](MoveTimer::total) for the red and blue times not as raw bytes, but as
	/// 	UTF-8 strings with a comma (`','`) after each time (2 in total)
	/// 3. Write the first (at most) `6 * n` moves (all [`GridMove::Place { pos: (x, y), .. }`](GridMove::Place))
	/// 	as `x` then `y`, both as raw bytes
	/// 4. Write the remaining moves (all [`GridMove::Move { from: (x1, y1), to: (x2, y), .. }`](GridMove::Move))
	/// 	as `x1`, `y1`, `x2`, then `y2`, all as raw bytes
	fn into(self) -> String {
		let n = char::from(self.game.n as u8);
		let out = format!("{},{},", self.times[0].total, self.times[1].total);
		let mut moves = Vec::new();
		for i in self.game.history.iter() {
			match i {
				GridMove::Place { pos: (x, y), .. } => moves.push(
					format!("{}{}", char::from(*x as u8), char::from(*y as u8))
				),
				GridMove::Move { from: (x1, y1), to: (x2, y2), .. } => moves.push(
					format!("{}{}{}{}", char::from(*x1 as u8), char::from(*y1 as u8), char::from(*x2 as u8), char::from(*y2 as u8))
				),
			}
		}
		format!("{}{}{}", n, out, moves.join(""))
	}
}

impl Application for Player {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = (AppConfig, GameSettings);
    
    fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let grid = Grid::from(flags.1);
        ( Self {
			config: flags.0.config_dir,
			game: grid,
			settings: flags.1,
			move_indicator: MoveIndicator(flags.1.first_move as usize),
			..Default::default()
		}, Command::none() )
    }
    
    fn title(&self) -> String {
        String::from("Star Crossed")
    }
    
    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        if let (true, index) = self.game.update(message.clone()) {
            if index == 1 { self.scores[self.game.n - N_MIN].right += 1 } else { self.scores[self.game.n - N_MIN].left += 1 }
        };
        match message {
			Message::Grid(GridMessage::SaveGameOverlay) => self.save_overlay = true,
			Message::Grid(GridMessage::SaveGameCancel) => self.save_overlay = false,
			Message::Grid(GridMessage::SaveGame) => {
				let mut f = PathBuf::from(&self.save_name);
				if f.iter().count() != 1 {
					self.save_error = Some("Cannot use '/' in save name".to_string());
					return Command::none()
				}
				f = self.config.join(f);
				f.set_extension("txt");
				if f.exists() {
					self.save_error = Some(format!("Save '{}' already exists", self.save_name));
					return Command::none()
				}
				match std::fs::OpenOptions::new().write(true).create(true).open(&f) {
					Ok(mut file) => {
						if let Err(e) = file.write(<&mut Self as Into<String>>::into(self).as_bytes()) {
							self.save_error = Some(format!("Save error: {}", e))
						} else {
							self.save_overlay = false;
							self.save_error = None;
						}
					}
					Err(e) => self.save_error = Some(format!("Save error: {}", e))
				}
			},
			Message::Grid(GridMessage::SaveGameUpdate(t)) => {
				self.save_name = t;
				self.save_error = None
			},
			Message::Grid(GridMessage::ExportFull) => {
				let out: String = self.into();
				if let Some(p) = rfd::FileDialog::new().add_filter("Text files", &["txt"]).save_file() {
					let _ = std::fs::write(p.with_extension("txt"), out);
				}
			}
			Message::Grid(GridMessage::CopyState) => return iced::clipboard::write(self.game.encode()),
            Message::Grid(GridMessage::MoveMarker(_, _, _)) | Message::Grid(GridMessage::PopulateMarker(_, _)) => {
                self.times[self.game.turn ^ 1].end();
                if !self.game.finished { self.times[self.game.turn].start(); }
                if !self.game.finished { self.move_indicator.0 = self.game.turn }
            }
            Message::Grid(GridMessage::Reset) => {
				self.move_indicator.0 = self.game.turn;
                self.times = [MoveTimer::new(), MoveTimer::new()]
            }
            _ => {}
        }
        Command::none()
    }
    
    fn view(&self) -> Element<'_, Self::Message, Renderer<Self::Theme>> {
        let game_view = col![
            self.game.view(Length::FillPortion(15)),
            self.game.message().height(Length::FillPortion(1))
        ].width(Length::FillPortion(9)).height(Length::Fill);
        let middle = row![
            self.times[0].view().width(Length::FillPortion(1)).vertical_alignment(Vertical::Center).style(Color::from_rgb(1., 0., 0.)),
            game_view,
            self.times[1].view().width(Length::FillPortion(1)).horizontal_alignment(Horizontal::Right).vertical_alignment(Vertical::Center).style(Color::from_rgb(0., 0., 1.)),
        ].padding(20)
            .width(Length::Fill);
		let top = row![PlayerMenu::view()].width(Length::Fill);
        let content = col![
			top,
            self.scores[self.game.n - N_MIN].view(Length::FillPortion(1)),
            middle.width(Length::Fill).height(Length::FillPortion(18)),
            self.move_indicator.view(Length::FillPortion(1))
        ].align_items(Alignment::Center);

		Modal::new(
			self.save_overlay,
			container(content)
				.width(Length::Fill)
				.height(Length::Fill)
				.center_x()
				.center_y(),
			|| {
				let top = row![
					text_input("Game name", &*self.save_name).width(Length::Fill).on_input(|t| Message::Grid(GridMessage::SaveGameUpdate(t))).on_submit(Message::Grid(GridMessage::SaveGame)),
					button(text("Save game")).width(Length::Shrink).on_press(Message::Grid(GridMessage::SaveGame))
				].spacing(10.);
				let inner = if let Some(err) = &self.save_error {
					col![
						top,
						text(err).width(Length::Fill).style(Color::new(1., 0., 0., 1.)),
					]
				} else {
					col![top]
				}.spacing(20.).align_items(Alignment::Center).width(Length::Fill);
				Card::new(text("Save Game").width(Length::Fill), inner).width(Length::Fixed(350.)).on_close(Message::Grid(GridMessage::SaveGameCancel)).into()
			}
		).into()
    }
    
    fn subscription(&self) -> Subscription<Self::Message> {
        every(Duration::from_millis(250)).map(|_| Message::Tick)
    }
}
