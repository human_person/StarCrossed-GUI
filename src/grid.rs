use iced::{Color, Element, keyboard, Length, mouse, Point, Rectangle, Size, widget, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced::keyboard::KeyCode;
use iced::mouse::Interaction;
use iced::widget::{Canvas, Text};
use iced::widget::canvas::{Cache, Cursor, Event, Frame, Geometry, Gradient, LineCap, LineDash, Path, Program, Stroke, Style};
use iced::widget::canvas::gradient::{ColorStop, Linear};
use iced_native::event::Status;
use iced_native::widget::text;
use itertools::Itertools;
use crate::{Message, N_MAX, N_MIN};
use crate::grid_settings::GameSettings;

/// # Game visualizer and logic
///
/// Contains all the logic required for playing a game
pub struct Grid {
	/// "Size" of the game. The grid is on a `2 * n + 1` grid, and you can place `3 * n` counters
	pub n: usize,
	/// Cache of the grid the markers are placed on
	pub grid_cache: Cache,
	/// Position of the markers
	pub markers: [Vec<(usize, usize)>; 2],
	/// Index of the currently selected marker
	pub selected: Option<usize>,
	/// Turn index. 0 for red, and 1 for blue
	pub turn: usize,
	/// List of valid moved for the selected marker. Saved to prevent recalculation every
	/// [draw](Program::draw) call
	pub valid: Vec<(usize, usize)>,
	/// Is the game finished
	pub finished: bool,
	/// The start and end of the line that finished the game
	pub finished_points: Option<((usize, usize), (usize, usize))>,
	/// History of the game
	pub history: Vec<GridMove>,
	/// Index of the viewed history. When playing, this is the length of [`history`](Grid::history)
	pub history_index: usize,
	/// Is the game being played at the moment
	pub playing: bool,
	/// If the grid being used to visualize, not play, a game
	pub visualiser: bool,
	pub alternate: bool,
	pub next_first_move: usize
}

impl Clone for Grid {
	fn clone(&self) -> Self {
		Self {
			n: self.n,
			grid_cache: Cache::new(),
			markers: self.markers.clone(),
			selected: self.selected,
			turn: self.turn,
			valid: self.valid.clone(),
			finished: self.finished,
			finished_points: self.finished_points,
			history: self.history.clone(),
			history_index: self.history_index,
			playing: self.playing,
			visualiser: false,
			alternate: true,
			next_first_move: 1
		}
	}
}

impl From<GameSettings> for Grid {
	fn from(value: GameSettings) -> Self {
		Self {
			n: value.first_n,
			turn: value.first_move as usize,
			alternate: value.alternate_moves,
			next_first_move: if value.alternate_moves { !value.first_move } else { value.first_move } as usize,
			..Default::default()
		}
	}
}

/// # Grid messaged
///
/// Messages for the game that are wrapped in a [`Message::Grid`]
#[derive(Debug, Clone, PartialEq)]
pub enum GridMessage {
	/// Reset the board, but not the scored
	Reset,
	/// Populate a position with a marker at `PopulateMarker(x, y)`
	PopulateMarker(usize, usize),
	/// Move one marker at index `i` to the point `(x, y)` for `MoveMarker(i, x, y)`
    MoveMarker(usize, usize, usize),
	/// Select the marker at the given index
    SelectMarker(usize),
	/// Deselect any selected marker
	DeselectMarker,
	/// Move back in the game history
    PrevHistory,
	/// Move forward in the game history
	NextHistory,
	/// Increase [`n`](Grid::n)
    IncreaseSize,
	/// Decrease [`n`](Grid::n)
	DecreaseSize,
	/// Export the full game with history and times to a text file
	ExportFull,
	/// Copy the board state to the clipboard
	CopyState,
	/// Save the current game to the [app data](dirs::config_dir) folder
	SaveGame,
	/// Update the running value of the saved game file name
	SaveGameUpdate(String),
	/// Show the save game overlay
	SaveGameOverlay,
	/// Cancel saving the game
	SaveGameCancel
}

/// # Grid movements
///
/// Movements used to store the history of a game
#[derive(Clone)]
pub enum GridMove {
	/// Marker was placed
	Place {
		/// Previous move marker positions before the current marker placement
		state: [Vec<(usize, usize)>; 2],
		/// Position the marker was placed at
		pos: (usize, usize),
		/// [Turn](Grid::turn) the current marker was played for
		turn: usize
	},
	/// Marker movement
	Move {
		/// Previous move marker positions before the marker was moved
		state: [Vec<(usize, usize)>; 2],
		/// Position marker was moved from
		from: (usize, usize),
		/// Position marker was moved to
		to: (usize, usize),
		/// [Turn](Grid::turn) the current marker was moved for
		turn: usize
	}
}

impl Default for Grid {
	fn default() -> Self {
		Self {
			n: 2,
			grid_cache: Cache::new(),
			markers: [Vec::new(), Vec::new()],
			selected: None, turn: 0,
			valid: Vec::new(),
			finished: false, finished_points: None,
			history: Vec::new(), playing: true, history_index: 0,
			visualiser: false,
			alternate: true, next_first_move: 1
		}
	}
}

impl Grid {
	/// View the grid. See [`Grid::draw`] for how it's drawn
	pub fn view(&self, height: Length) -> Element<Message> {
		Canvas::new(self)
			.width(Length::Fill)
			.height(height)
			.into()
	}
	
	/// Update `&mut self` with a [`Message`]
	pub fn update(&mut self, message: Message) -> (bool, usize) {
		let mut last_finished = self.finished.clone();
		let last_move = self.turn;
		match message {
			Message::Grid(GridMessage::PopulateMarker(x, y)) => {
				self.history.push(GridMove::Place {
					state: self.markers.clone(), pos: (x, y), turn: self.turn
				});
				self.history_index += 1;
				self.markers[self.turn].push((x, y));
				if self.markers[self.turn].len() >= 2 * self.n + 1 {
					self.check_win()
				}
				self.turn ^= 1;
				self.selected = None;
				self.valid.clear();
			}
			Message::Grid(GridMessage::MoveMarker(i, x, y)) => {
				self.history.push(GridMove::Move {
					state: self.markers.clone(),
					from: self.markers[self.turn][i],
					to: (x, y),
					turn: self.turn
				});
				self.history_index += 1;
				self.markers[self.turn][i] = (x, y);
				self.check_win();
				self.turn ^= 1;
				self.selected = None;
				self.valid.clear();
			}
			Message::Grid(GridMessage::SelectMarker(i)) => {
				self.selected = Some(i);
				self.update_valid();
			},
			Message::Grid(GridMessage::DeselectMarker) => {
				self.selected = None;
				self.valid.clear();
			},
			Message::Grid(GridMessage::NextHistory) => {
				if self.playing { return (false, last_move) }
				self.history_index += 1;
				self.playing = self.history.len() == self.history_index
			}
			Message::Grid(GridMessage::PrevHistory) => {
				if self.history_index == 0 { return (false, last_move) }
				self.history_index -= 1;
				self.playing = false
			}
			Message::Grid(GridMessage::Reset) => self.reset(&mut last_finished),
			Message::Grid(GridMessage::IncreaseSize) => {
				if self.n == N_MAX { return (false, last_move) }
				self.n += 1;
				self.reset(&mut last_finished)
			}
			Message::Grid(GridMessage::DecreaseSize) => {
				if self.n == N_MIN { return (false, last_move) }
				self.n -= 1;
				self.reset(&mut last_finished)
			}
			_ => {}
		}
		(self.finished != last_finished, last_move)
	}
	
	fn reset(&mut self, last_finished: &mut bool) {
		self.grid_cache.clear();
		self.valid.clear();
		self.finished_points = None;
		self.finished = false;
		self.selected = None;
		if self.alternate {
			self.turn = self.next_first_move;
			self.next_first_move ^= 1;
		} else {
			self.turn = self.next_first_move
		};
		self.markers[0].clear();
		self.markers[1].clear();
		*last_finished = false;
		self.history_index = 0;
		self.history.clear();
		self.playing = true;
	}
	
	/// check if a position on the grid is occupied
	fn is_occupied(&self, x: usize, y: usize) -> bool {
		self.markers[0].iter().position(|(i, j)| *i == x && *j == y).is_some() ||
			self.markers[1].iter().position(|(i, j)| *i == x && *j == y).is_some()
	}
	
	/// Update the valid movements for the selected marked. Called only when needed
	///
	/// > This function is quit messy and brute-forces calculating a lot of movements, but it works
	fn update_valid(&mut self) {
		self.valid.clear();
		let (i, j) = if let Some(s) = self.selected {
			self.markers[self.turn][s]
		} else { return };
		// get the horizontal movements
		match i {
			0 => self.valid.push((i + 1, j)),
			t if t == 2 * self.n => self.valid.push((i - 1, j)),
			_ => {
				self.valid.push((i + 1, j));
				self.valid.push((i - 1, j));
			}
		}
		// get the vertical movements
		match j {
			0 => self.valid.push((i, j + 1)),
			t if t == 2 * self.n => self.valid.push((i, j - 1)),
			_ => {
				self.valid.push((i, j + 1));
				self.valid.push((i, j - 1));
			}
		}
		// outwards diagonal movements (movements diagonally away from the center)
		if i < self.n {
			if j < self.n { self.valid.push((i + 1, j + 1)) }
			if j > self.n { self.valid.push((i + 1, j - 1)) }
		}
		if i > self.n {
			if j < self.n { self.valid.push((i - 1, j + 1)) }
			if j > self.n { self.valid.push((i - 1, j - 1)) }
		}
		// inwards diagonal movements (movements diagonally towards from the center)
		if i > 0 && i <= self.n {
			if j > 0 && j <= self.n { self.valid.push((i - 1, j - 1)) }
			if j < 2 * self.n && j >= self.n { self.valid.push((i - 1, j + 1)) }
		}
		if i < 2 * self.n && i >= self.n {
			if j > 0 && j <= self.n { self.valid.push((i + 1, j - 1)) }
			if j < 2 * self.n && j >= self.n { self.valid.push((i + 1, j + 1)) }
		}
		self.valid = self.valid.iter().filter(|(x, y)| !self.is_occupied(*x, *y)).map(|t| *t).collect()
	}
	
	/// Check if the current grid state contains a line the right length and thus a win
	pub fn check_win(&mut self) {
		// sort the markers by row then column to ensure the first element will always be the
		// highest and leftmost, and the last will be the lowest and rightmost
		let mut markers = self.markers[self.turn].clone();
		markers.sort();
		'main: for comb in markers.iter().combinations(self.n * 2 + 1) {
			let first = comb[0];
			let second = comb[1];
			let rule: Box<dyn Fn(&(usize, usize), usize) -> bool> = if first.0 == second.0 {
				Box::new(|i: &(usize, usize), _n: usize| i.0 == first.0)
			} else if first.1 == second.1 {
				Box::new(|i: &(usize, usize), _n: usize| i.1 == first.1)
			} else if first.0 == 0 && first.1 == 0 && second.0 == 1 && second.1 == 1 {
				Box::new(|i: &(usize, usize), n: usize| *i == (n, n))
			} else if first.0 == 0 && first.1 == self.n * 2 && second.0 == 1 && second.1 == self.n * 2 - 1 {
				Box::new(|i: &(usize, usize), n: usize| *i == (n, self.n * 2 - n))
			} else { return };
			for (n, p) in comb[2..].iter().enumerate() {
				if !rule(*p, n + 2) { continue 'main }
			}
			self.finished_points = Some((*first, *comb[2 * self.n]));
			self.finished = true;
			return
		}
		self.finished = false
	}
	
	/// Message to accompany the grid state. Displayed under the grid when playing a game
	pub fn message(&self) -> Text {
		if !self.playing {
			text(format!("Viewing move {}/{}", self.history_index + 1, self.history.len()))
		} else if self.finished {
			text(format!("{} wins!", if self.turn == 1 { "Red" } else { "Blue" }))
		} else {
			text("")
		}.width(Length::Fill).vertical_alignment(Vertical::Center).horizontal_alignment(Horizontal::Center)
	}
	
	/// Encode the board state (no history or times) into a single string
	///
	/// The first letter indicates the current move, with the remaining letters iterating over the
	/// grid and being `'r'` for a red marked, `'b'` for a blue marker, and `'_'` for an empty
	/// space. The iteration is done in rows
	pub fn encode(&self) -> String {
		let k = self.n * 2 + 1;
		let mut out = vec!["_"; k * k];
		if self.playing {
			for (x, y) in self.markers[0].iter() {
				out[*x * k + *y] = "r"
			}
			for (x, y) in self.markers[1].iter() {
				out[*x * k + *y] = "b"
			}
		} else {
			let state = match &self.history[self.history_index] {
				GridMove::Place { state, .. } => state,
				GridMove::Move { state, .. } => state,
			};
			for (x, y) in state[0].iter() {
				out[*x * k + *y] = "r"
			}
			for (x, y) in state[1].iter() {
				out[*x * k + *y] = "b"
			}
		}
		format!("{}{}", if self.turn == 0 { 'r' } else { 'b' }, out.join(""))
	}
}

/// Get the position of the cursor within the grid.
///
/// For each point, place a circle around it, inside which we view as being over the point
fn get_pos(cursor: Point, center: Point, radius: f32, spacing: f32, n: usize) -> Option<(u8, u8)> {
	let n = n as i8;
	for i in -n..=n {
		for j in -n..=n {
			if Point::new(center.x + spacing * i as f32, center.y + spacing * j as f32).distance(cursor) < radius {
				return Some(((i + n) as u8, (j + n) as u8))
			}
		}
	}
	None
}

impl Program<Message> for Grid {
	type State = ();
	
	fn update(&self, _state: &mut Self::State, event: Event, bounds: Rectangle, cursor: Cursor) -> (Status, Option<Message>) {
		if self.visualiser { return (Status::Ignored, None) }
		match event {
			Event::Mouse(mouse::Event::ButtonReleased(mouse::Button::Left)) => {
				if self.finished || !self.playing { return (Status::Ignored, None) }
				let cursor = if let Some(p) = cursor.position() {
					p
				} else {
					return (Status::Ignored, None)
				};
				let size = bounds.width.min(bounds.height) / (2. * self.n as f32 + 0.8);
				if let Some((x, y)) = get_pos(
					cursor, bounds.center(),
					size * 0.4, size, self.n
				) {
					let x = x as usize;
					let y = y as usize;
					if self.markers[self.turn].len() == 3 * self.n {
						if let Some(i) = self.selected {
							if self.is_occupied(x, y) {
								if let Some(index) = self.markers[self.turn].iter().position(|(i, j)| *i == x && *j == y) {
									if index == i {
										(Status::Captured, Some(Message::Grid(GridMessage::DeselectMarker)))
									} else{
										(Status::Captured, Some(Message::Grid(GridMessage::SelectMarker(index))))
									}
								} else {
									(Status::Captured, Some(Message::Grid(GridMessage::DeselectMarker)))
								}
							} else {
								if self.valid.iter().position(|(i, j)| *i == x && *j == y).is_some() {
									(Status::Captured, Some(Message::Grid(GridMessage::MoveMarker(i, x, y))))
								} else {
									(Status::Captured, Some(Message::Grid(GridMessage::DeselectMarker)))
								}
							}
						} else {
							if let Some(index) = self.markers[self.turn].iter().position(|(i, j)| *i == x && *j == y) {
								(Status::Captured, Some(Message::Grid(GridMessage::SelectMarker(index))))
							} else {
								(Status::Captured, Some(Message::Grid(GridMessage::DeselectMarker)))
							}
						}
					} else {
						if !self.is_occupied(x, y) {
							(Status::Captured, Some(Message::Grid(GridMessage::PopulateMarker(x, y))))
						} else {
							(Status::Captured, None)
						}
					}
				} else {
					(Status::Ignored, None)
				}
			}
			Event::Keyboard(keyboard::Event::KeyPressed {
				key_code: KeyCode::R, ..
			}) => (Status::Captured, Some(Message::Grid(GridMessage::Reset))),
			Event::Keyboard(keyboard::Event::KeyPressed {
				key_code: KeyCode::Left, ..
			}) => (Status::Captured, Some(Message::Grid(GridMessage::PrevHistory))),
			Event::Keyboard(keyboard::Event::KeyPressed {
				key_code: KeyCode::Right, ..
			}) => (Status::Captured, Some(Message::Grid(GridMessage::NextHistory))),
			Event::Keyboard(keyboard::Event::KeyPressed {
				key_code: KeyCode::Up, ..
			}) => (Status::Captured, Some(Message::Grid(GridMessage::IncreaseSize))),
			Event::Keyboard(keyboard::Event::KeyPressed {
				key_code: KeyCode::Down, ..
			}) => (Status::Captured, Some(Message::Grid(GridMessage::DecreaseSize))),
			_ => (Status::Ignored, None)
		}
	}
	
	fn draw(&self, _state: &Self::State, theme: &Theme, bounds: Rectangle, _cursor: Cursor) -> Vec<Geometry> {
		let grid = self.grid_cache.draw(bounds.size(), |f| {
			let size = f.width().min(f.height()) / (2. * self.n as f32 + 0.8);
			let center = f.center();
			
			let style = Stroke {
				style: Style::Solid(theme.palette().text),
				width: 3.0,
				line_cap: LineCap::Round,
				..Default::default()
			};
			
			// squares
			for i in 1..=self.n {
				let s = Path::rectangle(Point::new(center.x - i as f32 * size, center.y - i as f32 * size), Size::new(size * 2. * i as f32, size * 2. * i as f32));
				f.stroke(&s, style.clone());
			}
			
			// full length diagonal
			let mut p = Path::line(Point::new(center.x - self.n as f32 * size, center.y - self.n as f32 * size), Point::new(center.x + self.n as f32 * size, center.y + self.n as f32 * size));
			f.stroke(&p, style.clone());
			p = Path::line(Point::new(center.x - self.n as f32 * size, center.y + self.n as f32 * size), Point::new(center.x + self.n as f32 * size, center.y - self.n as f32 * size));
			f.stroke(&p, style.clone());
			p = Path::line(Point::new(center.x - self.n as f32 * size, center.y), Point::new(center.x + self.n as f32 * size, center.y));
			f.stroke(&p, style.clone());
			p = Path::line(Point::new(center.x, center.y - self.n as f32 * size), Point::new(center.x, center.y + self.n as f32 * size));
			f.stroke(&p, style.clone());
			let left = center.x - self.n as f32 * size;
			let right = center.x + self.n as f32 * size;
			let top = center.y + self.n as f32 * size;
			let bottom = center.y - self.n as f32 * size;
			for i in 1..self.n {
				let inset = i as f32 * size;
				// remaining vertical and horizontal lines
				p = Path::line(Point::new(left, center.y - inset), Point::new(right, center.y - inset)); // bottom row
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(left, center.y + inset), Point::new(right, center.y + inset)); // top row
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x - inset, top), Point::new(center.x - inset, bottom)); // left column
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x + inset, top), Point::new(center.x + inset, bottom)); // right column
				f.stroke(&p, style.clone());
				// carets (>) on the sides of the board
				p = Path::line(Point::new(left, center.y + inset), Point::new(left + inset, center.y));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(left, center.y - inset), Point::new(left + inset, center.y));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(right, center.y + inset), Point::new(right - inset, center.y));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(right, center.y - inset), Point::new(right - inset, center.y));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x - inset, top), Point::new(center.x, top - inset));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x + inset, top), Point::new(center.x, top - inset));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x - inset, bottom), Point::new(center.x, bottom + inset));
				f.stroke(&p, style.clone());
				p = Path::line(Point::new(center.x + inset, bottom), Point::new(center.x, bottom + inset));
				f.stroke(&p, style.clone());
			}
		});
		
		let mut f = Frame::new(bounds.size());
		
		let size = f.width().min(f.height()) / (2. * self.n as f32 + 0.8);
		let center = f.center();
		let mut p = Path::circle(Point::ORIGIN, 0.);
		
		let (red, red_sat, blue, blue_sat) = match theme {
			Theme::Light => (
				Color::new(1., 0., 0., 0.8), Color::new(1., 0., 0., 1.),
				Color::new(0., 0., 1., 0.8), Color::new(0., 0., 1., 1.)
			),
			_ => (
				Color::new(1., 0., 0., 0.8), Color::new(1., 0., 0., 1.),
				Color::new(0., 0., 1., 0.8), Color::new(0., 0., 1., 1.)
			)
		};
		
		if self.playing {
			if let Some(((x1, y1), (x2, y2))) = self.finished_points {
				p = Path::line(
					Point::new(center.x - (self.n as f32 - x1 as f32) * size, center.y - (self.n as f32 - y1 as f32) * size),
					Point::new(center.x - (self.n as f32 - x2 as f32) * size, center.y - (self.n as f32 - y2 as f32) * size)
				);
				let mut col = theme.palette().text;
				col.a = 0.5;
				f.stroke(&p, Stroke {
					style: Style::Solid(col),
					width: 25.0,
					line_cap: LineCap::Round,
					line_join: Default::default(),
					line_dash: Default::default(),
				});
			}
		}
		
		let mut draw_points = |points: &[Vec<(usize, usize)>; 2]| {
			let tl = Point::new(center.x - size * self.n as f32, center.y - size * self.n as f32);
			for (k, (x, y)) in points[0].iter().enumerate() {
				let origin = Point::new(tl.x + *x as f32 * size, tl.y + *y as f32 * size);
				let radius_scale = if self.turn == 0 && Some(k) == self.selected { 0.25 } else { 0.2 };
				p = Path::circle(origin, size * radius_scale);
				f.fill(&p, red);
				f.stroke(&p, Stroke {
					style: Style::Solid(red_sat),
					width: 3.,
					..Default::default()
				});
				f.fill_text(widget::canvas::Text {
					content: char::from(65 + k as u8).to_string(),
					position: Point::new(origin.x - size * (radius_scale + 0.02), origin.y - 2.),
					color: red_sat,
					vertical_alignment: Vertical::Bottom,
					horizontal_alignment: Horizontal::Right,
					..Default::default()
				})
			}
			for (k, (x, y)) in points[1].iter().enumerate() {
				let origin = Point::new(tl.x + *x as f32 * size, tl.y + *y as f32 * size);
				let radius_scale = if self.turn == 1 && Some(k) == self.selected { 0.25 } else { 0.2 };
				p = Path::circle(origin, size * radius_scale);
				f.fill(&p, blue);
				f.stroke(&p, Stroke {
					style: Style::Solid(blue_sat),
					width: 3.,
					..Default::default()
				});
				f.fill_text(widget::canvas::Text {
					content: char::from(65 + k as u8).to_string(),
					position: Point::new(origin.x - size * (radius_scale + 0.02), origin.y - 2.),
					color: blue_sat,
					vertical_alignment: Vertical::Bottom,
					horizontal_alignment: Horizontal::Right,
					..Default::default()
				})
			}
		};
		
		let tl = Point::new(center.x - size * self.n as f32, center.y - size * self.n as f32);
		if self.playing {
			// markers
			draw_points(&self.markers);
			
			// valid moves
			for (x, y) in self.valid.iter() {
				let origin = Point::new(tl.x + *x as f32 * size, tl.y + *y as f32 * size);
				p = Path::circle(origin, size * 0.1);
				f.fill(&p, Color::new(0., 1., 0., 0.8));
			}
		} else {
			match &self.history[self.history_index] {
				GridMove::Place { pos: (x, y), turn, state } => {
					draw_points(&state);
					let origin = Point::new(tl.x + *x as f32 * size, tl.y + *y as f32 * size);
					p = Path::circle(origin, size * 0.2);
					let mut fill = if *turn == 0 { Color::new(1., 0., 0., 0.8) } else { Color::new(0., 0., 1., 0.8) };
					f.fill(&p, fill);
					fill.a = 1.0;
					f.stroke(&p, Stroke {
						style: Style::Solid(fill),
						width: 3.,
						line_dash: LineDash {
							segments: &[10., 5.],
							offset: 0,
						},
						..Default::default()
					});
					f.fill_text(widget::canvas::Text {
						content: char::from(65 + state[*turn].len() as u8).to_string(),
						position: Point::new(origin.x - size * 0.22, origin.y - 2.),
						color: fill,
						vertical_alignment: Vertical::Bottom,
						horizontal_alignment: Horizontal::Right,
						..Default::default()
					});
				}
				GridMove::Move { from: (x1, y1), to: (x2, y2), turn, state } => {
					draw_points(&state);
					let origin1 = Point::new(tl.x + *x1 as f32 * size, tl.y + *y1 as f32 * size);
					let origin2 = Point::new(tl.x + *x2 as f32 * size, tl.y + *y2 as f32 * size);
					p = Path::circle(origin2, size * 0.2);
					let mut fill = if *turn == 0 { Color::new(1., 0., 0., 0.8) } else { Color::new(0., 0., 1., 0.8) };
					f.fill(&p, fill);
					fill.a = 1.0;
					f.stroke(&p, Stroke {
						style: Style::Solid(fill),
						width: 3.,
						line_dash: LineDash {
							segments: &[10., 5.],
							offset: 0,
						},
						..Default::default()
					});
					
					p = Path::line(origin1, origin2);
					let style = Style::Gradient(Gradient::Linear(Linear {
						start: origin1,
						end: origin2,
						color_stops: vec![
							ColorStop { offset: 0.0, color: Color::new(0., 0., 0., 0.), },
							ColorStop { offset: 0.2, color: Color::from_rgba8(214, 202, 26, 1.0), },
							ColorStop { offset: 1.0, color: Color::from_rgba8(183, 255, 0, 1.0), },
						],
					}));
					f.stroke(&p, Stroke {
						style,
						width: 5.,
						line_cap: LineCap::Round,
						..Default::default()
					});
				}
			}
		}
		
		vec![grid, f.into_geometry()]
	}
	
	fn mouse_interaction(&self, _state: &Self::State, bounds: Rectangle, cursor: Cursor) -> Interaction {
		if self.finished || !self.playing || self.visualiser { return Interaction::default() }
		if let Some(p) = cursor.position() {
			let size = bounds.width.min(bounds.height) / (2. * self.n as f32 + 0.8);
			if get_pos(p, bounds.center(), size * 0.4, size, self.n).is_some() {
				return Interaction::Pointer
			}
		}
		Interaction::default()
	}
}
