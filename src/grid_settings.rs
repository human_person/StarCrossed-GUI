use iced::{Element, Length, Renderer, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced_native::{column as col, row};
use iced_native::widget::{button, container, horizontal_space, radio, slider, text};
use crate::menu::MenuMessage;
use crate::{Message, N_MAX, N_MIN};

/// Settings for the game
#[derive(Copy, Clone)]
pub struct GameSettings {
	pub alternate_moves: bool,
	pub first_move: bool,
	pub first_n: usize
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum NewOptionMessage {
	UpdateN(usize),
	SetAlternate(bool),
	SetFirstMove(bool)
}

impl Default for GameSettings {
	fn default() -> Self {
		Self { alternate_moves: true, first_move: false, first_n: 2 }
	}
}

impl<'a> GameSettings {
	pub fn update(&mut self, m: NewOptionMessage) {
		match m {
			NewOptionMessage::UpdateN(v) => self.first_n = v,
			NewOptionMessage::SetAlternate(t) => self.alternate_moves = t,
			NewOptionMessage::SetFirstMove(t) => self.first_move = t,
		}
	}
	
	pub fn view(&self) -> Element<'a, Message, Renderer<Theme>> {
		let top = button(text("Back")).on_press(Message::Menu(MenuMessage::Home));
		let values = col![
			row![text("Starting size"), horizontal_space(Length::Fill), text(self.first_n.to_string())],
			slider((N_MIN as u32)..=(N_MAX as u32), self.first_n as u32, |v| Message::NewOptions(NewOptionMessage::UpdateN(v as usize))).step(1),
		].spacing(20).width(Length::Fill);
		let alternate_radio = if self.alternate_moves {
			col![
				text("Alternate first move:"),
				radio("Yes", (), Some(()), |_| Message::NewOptions(NewOptionMessage::SetAlternate(true))),
				radio("No", (), None, |_| Message::NewOptions(NewOptionMessage::SetAlternate(false)))
			]
		} else {
			col![
				text("Alternate first move:"),
				radio("Yes", (), None, |_| Message::NewOptions(NewOptionMessage::SetAlternate(true))),
				radio("No", (), Some(()), |_| Message::NewOptions(NewOptionMessage::SetAlternate(false)))
			]
		}.spacing(20).width(Length::Fill);
		let first_move = if self.first_move {
			col![
				text("First move:"),
				radio("Red", (), None, |_| Message::NewOptions(NewOptionMessage::SetFirstMove(false))),
				radio("Blue", (), Some(()), |_| Message::NewOptions(NewOptionMessage::SetFirstMove(true)))
			]
		} else {
			col![
				text("First move:"),
				radio("Red", (), Some(()), |_| Message::NewOptions(NewOptionMessage::SetFirstMove(false))),
				radio("Blue", (), None, |_| Message::NewOptions(NewOptionMessage::SetFirstMove(true)))
			]
		}.spacing(20).width(Length::Fill);
		let finish = container(button(text("Play")).on_press(Message::Menu(MenuMessage::Play)))
			.align_x(Horizontal::Right).align_y(Vertical::Bottom)
			.width(Length::Fill).height(Length::Fill);
		let inner = row![
			horizontal_space(Length::FillPortion(3)),
			col![values, alternate_radio, first_move, finish].spacing(30).width(Length::FillPortion(5)),
			horizontal_space(Length::FillPortion(3)),
		].height(Length::Fill).padding(20);
		col![top, inner].into()
	}
}