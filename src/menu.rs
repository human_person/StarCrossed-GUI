use std::collections::HashMap;
use std::path::PathBuf;
use chrono::{DateTime, Local};
use iced::{Alignment, Application, Color, Element, executor, Length, Renderer, Theme};
use iced::alignment::{Horizontal, Vertical};
use iced::theme::Button;
use iced::widget::Column;
use iced_aw::{Card, Modal};
use iced_native::{Command, column as col, row};
use iced_native::widget::{button, container, scrollable, text, text_input};
use itertools::Itertools;
use crate::{AppConfig, Message};
use crate::grid_settings::GameSettings;
use crate::player::Player;

pub struct HomeMenu {
	config_dir: PathBuf,
	pub local_saved: HashMap<PathBuf, (Button, String, DateTime<Local>)>,
	pub visualised_grid: Option<Result<Player, String>>,
	state: MenuState,
	pub history_input: Option<Result<Player, ()>>,
	pub custom_state: String,
	pub selected: Option<PathBuf>,
	is_editing: bool,
	selected_filename: String,
	edit_input_err: Option<String>,
	pub new_options: GameSettings
}

enum MenuState {
	Main, CustomChoose, PlaySettings,
	CustomState,
	Help,
	Open
}

#[derive(Debug, Clone, PartialEq)]
pub enum MenuMessage {
	PlaySettings, Play,
	CustomSettingsBack,
	PlayCustom,
	PlayCustomHistory,
	SelectCustomState, PlayCustomState, SetCustomState(String),
	Help, Home,
	Open, DeleteSelected,
	OpenCloseEdit(bool), EditName(String), FinaliseModifyName
}

impl Default for HomeMenu {
	fn default() -> Self {
		Self {
			config_dir: PathBuf::new(), local_saved: HashMap::new(),
			state: MenuState::Main,
			history_input: None, custom_state: String::new(),
			visualised_grid: None, selected: None,
			is_editing: false, selected_filename: String::new(),
			edit_input_err: None,
			new_options: Default::default(),
		}
	}
}

impl Application for HomeMenu {
	type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = AppConfig;
	
	fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
		( Self {
			config_dir: flags.config_dir,
			..Default::default()
		}, Command::none() )
	}
	
	fn title(&self) -> String { String::new() }
	
	fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
		if let Message::NewOptions(n) = &message {
			self.new_options.update(*n)
		}
		match message {
			Message::Menu(MenuMessage::PlaySettings) => self.state = MenuState::PlaySettings,
			Message::Menu(MenuMessage::Home) => {
				self.selected = None;
				self.visualised_grid = None;
				self.state = MenuState::Main
			},
			Message::Menu(MenuMessage::OpenCloseEdit(m)) => self.is_editing = m,
			Message::Menu(MenuMessage::Help) => self.state = MenuState::Help,
			Message::Menu(MenuMessage::Open) => {
				self.state = MenuState::Open;
				let list = std::fs::read_dir(&self.config_dir).unwrap();
				for (name, created, path) in list.filter_map(|t| t.ok())
					.filter_map(|t| if let Ok(m) = t.metadata() { Some((t, m)) } else { None })
					.filter(|(_, m)| m.is_file())
					.filter_map(|(t, m)| if let Ok(m) = m.created() { Some((t, m)) } else { None })
					.filter_map(|(t, m)| {
						let name = t.file_name().to_string_lossy().to_string();
						if name.ends_with(".txt") {
							Some((name[0..name.len() - 4].to_string(), m, t.path()))
						} else { None }
					})
					.sorted_by_key(|i| (i.1, i.0.clone())) {
					self.local_saved.insert(path, (
						Button::Secondary, name, DateTime::<Local>::from(created)
					));
				}
			},
			Message::Menu(MenuMessage::PlayCustom) => self.state = MenuState::CustomChoose,
			Message::Menu(MenuMessage::CustomSettingsBack) => self.state = MenuState::Main,
			Message::Menu(MenuMessage::PlayCustomHistory) => {
				if let Some(f) = rfd::FileDialog::new().add_filter("Text files", &["txt"]).pick_file() {
					match std::fs::read_to_string(f) {
						Ok(inner) => {
							match Player::try_from((inner, self.config_dir.clone())) {
								Ok(grid) => self.history_input = Some(Ok(grid)),
								Err(_) => self.history_input = Some(Err(())),
							}
						}
						Err(_) => self.history_input = Some(Err(())),
					}
				} else {
					self.history_input = Some(Err(()))
				}
			}
			Message::Menu(MenuMessage::SelectCustomState) => self.state = MenuState::CustomState,
			Message::Menu(MenuMessage::SetCustomState(t)) => self.custom_state = t,
			Message::Menu(MenuMessage::DeleteSelected) => {
				if let Some(p) = &self.selected {
					if let Err(e) = std::fs::remove_file(p) {
						self.edit_input_err = Some(format!("Delete error: {}", e))
					} else {
						self.local_saved.remove(p);
						self.selected = None;
						self.visualised_grid = None;
						self.is_editing = false;
						self.edit_input_err = None
					}
				}
			}
			Message::Menu(MenuMessage::EditName(s)) => {
				self.edit_input_err = None;
				self.selected_filename = s
			},
			Message::Menu(MenuMessage::FinaliseModifyName) => {
				if let Some(original) = &self.selected {
					let new = self.config_dir.join(&self.selected_filename).with_extension("txt");
					if let Err(e) = std::fs::rename(original, &new) {
						self.edit_input_err = Some(format!("Cannot rename: {}", e))
					} else {
						let mut data = self.local_saved.remove(original).unwrap();
						data.1 = self.selected_filename.clone();
						self.local_saved.insert(new.clone(), data);
						self.selected = Some(new)
					}
				}
			}
			Message::SelectFile(path) => {
				if let Some(path) = &self.selected {
					self.local_saved.entry(path.clone()).and_modify(|(s, _, _)| if let Button::Primary = *s { *s = Button::Secondary });
				}
				self.selected = Some(path.clone());
				match std::fs::read_to_string(path.clone()) {
					Ok(contents) => {
						if let Ok(mut player) = Player::try_from((contents, PathBuf::new())) {
							player.game.visualiser = true;
							self.visualised_grid = Some(Ok(player));
							self.selected_filename = self.local_saved.get(&path).unwrap().1.clone();
							self.local_saved.entry(path).and_modify(|(s, _, _)| *s = Button::Primary);
						} else {
							self.visualised_grid = Some(Err("Unable to parse save file".to_string()));
							self.local_saved.entry(path).and_modify(|(s, _, _)| *s = Button::Destructive);
						}
					}
					Err(e) => {
						self.visualised_grid = Some(Err(format!("Unable to read save file:\n{}", e)));
						self.local_saved.entry(path).and_modify(|(s, _, _)| *s = Button::Destructive);
					}
				}
			}
			_ => {}
		}
		Command::none()
	}
	
	fn view(&self) -> Element<'_, Self::Message, Renderer<Self::Theme>> {
		match self.state {
			MenuState::Main => {
				let buttons = col![
					button(text("New").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::PlaySettings)).width(Length::Fixed(200.)),
					button(text("Open").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Open)).width(Length::Fixed(200.)),
					button(text("New (Custom)").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::PlayCustom)).width(Length::Fixed(200.)),
					button(text("Help").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Help)).width(Length::Fixed(200.)),
					button(text("Quit").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Quit).width(Length::Fixed(200.)),
					button(text("Change theme").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::ChangeTheme).width(Length::Fixed(200.)),
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::FillPortion(8))
					.align_items(Alignment::Center);
				let inner = col![
					text("Star Crossed").size(40).width(Length::Fill).height(Length::FillPortion(5))
						.horizontal_alignment(Horizontal::Center).vertical_alignment(Vertical::Center),
					buttons
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::Fill)
					.align_items(Alignment::Center);
				container(inner).align_x(Horizontal::Center).align_y(Vertical::Center).into()
			}
			MenuState::CustomChoose => {
				let custom_buttons = col![
					button(text("New (Custom)").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::CustomSettingsBack)).width(Length::Fixed(200.)),
					row![row![].width(Length::Fixed(50.)), button(text("From history").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::PlayCustomHistory)).width(Length::Fixed(150.))],
					row![row![].width(Length::Fixed(50.)), button(text("From state").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::SelectCustomState)).width(Length::Fixed(150.))],
				].spacing(5.)
					.width(Length::Fill).height(Length::Shrink)
					.align_items(Alignment::Center);
				let buttons = col![
					button(text("New").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::PlaySettings)).width(Length::Fixed(200.)),
					button(text("Open").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Open)).width(Length::Fixed(200.)),
					custom_buttons,
					button(text("Help").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Help)).width(Length::Fixed(200.)),
					button(text("Quit").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Quit).width(Length::Fixed(200.)),
					button(text("Change theme").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::ChangeTheme).width(Length::Fixed(200.)),
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::FillPortion(8))
					.align_items(Alignment::Center);
				let inner = col![
					text("Star Crossed").size(40).width(Length::Fill).height(Length::FillPortion(5))
						.horizontal_alignment(Horizontal::Center).vertical_alignment(Vertical::Center),
					buttons
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::Fill)
					.align_items(Alignment::Center);
				container(inner).align_x(Horizontal::Center).align_y(Vertical::Center).into()
			}
			MenuState::CustomState => {
				let state_col = col![
					button(text("From state").horizontal_alignment(Horizontal::Center)).width(Length::Fixed(150.)),
					text_input("Board state", &*self.custom_state).on_submit(Message::Menu(MenuMessage::PlayCustomState)).width(Length::Fixed(150.)).on_input(|t| Message::Menu(MenuMessage::SetCustomState(t)))
				].spacing(5.);
				let custom_buttons = col![
					button(text("New (Custom)").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::CustomSettingsBack)).width(Length::Fixed(200.)),
					row![row![].width(Length::Fixed(50.)), button(text("From history").width(Length::Fill).horizontal_alignment(Horizontal::Center)).width(Length::Fixed(150.)).on_press(Message::Menu(MenuMessage::PlayCustomHistory))],
					row![row![].width(Length::Fixed(50.)), state_col],
				].spacing(5.)
					.width(Length::Fill).height(Length::Shrink)
					.align_items(Alignment::Center);
				let buttons = col![
					button(text("New").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::PlaySettings)).width(Length::Fixed(200.)),
					button(text("Open").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Open)).width(Length::Fixed(200.)),
					custom_buttons,
					button(text("Help").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Menu(MenuMessage::Help)).width(Length::Fixed(200.)),
					button(text("Quit").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::Quit).width(Length::Fixed(200.)),
					button(text("Change theme").width(Length::Fill).horizontal_alignment(Horizontal::Center)).on_press(Message::ChangeTheme).width(Length::Fixed(200.)),
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::FillPortion(8))
					.align_items(Alignment::Center);
				let inner = col![
					text("Star Crossed").size(40).width(Length::Fill).height(Length::FillPortion(5))
						.horizontal_alignment(Horizontal::Center).vertical_alignment(Vertical::Center),
					buttons
				].padding(20.)
					.spacing(20.)
					.width(Length::Fill).height(Length::Fill)
					.align_items(Alignment::Center);
				container(inner).align_x(Horizontal::Center).align_y(Vertical::Center).into()
			}
			MenuState::Help => {
				let top = row![button(text("Back").width(Length::Shrink)).on_press(Message::Menu(MenuMessage::Home)).height(Length::Shrink)].width(Length::Fill).height(Length::Shrink);
				let inner = col![
					top, container(text(include_str!("help.txt")).width(Length::Fill).height(Length::Fill)).padding(20.)
				].width(Length::Fill).height(Length::Fill);
				container(inner).align_x(Horizontal::Center).align_y(Vertical::Center).into()
			}
			MenuState::Open => {
				let top = container(button("Back").on_press(Message::Menu(MenuMessage::Home))).padding([0, 0, 5, 0]).width(Length::Fill);
				let mut items: Column<'_, Message, Renderer> = Column::new().spacing(5.).height(Length::Shrink).width(Length::Fill);
				for (path, (style, name, created)) in self.local_saved.iter() {
					let mut insert = button(row![
						text(name),
						text(created.format("%c")).width(Length::Fill).horizontal_alignment(Horizontal::Right),
					].height(Length::Shrink).width(Length::Fill));
					insert = match style {
						Button::Destructive => insert.on_press(Message::SelectFile(path.clone()))
							.style(Button::Destructive),
						Button::Primary => insert.on_press(Message::OpenFile)
								.style(Button::Primary),
						_ => insert.on_press(Message::SelectFile(path.clone()))
								.style(Button::Secondary),
					};
					items = items.push(insert);
				}
				let item_container = scrollable(row![items, text("").width(Length::Fixed(15.))]).height(Length::Fill).width(Length::Fill);
				let visualize = if let Some(p_res) = &self.visualised_grid {
					match p_res {
						Ok(p) => {
							let state = if p.game.finished {
								text(format!("{} has won", if p.game.turn == 1 { "Red" } else { "Blue" }))
							} else {
								text(format!("{} move", if p.game.turn == 0 { "Red" } else { "Blue" }))
							}.width(Length::FillPortion(1));
							col![
							p.game.view(Length::FillPortion(8)),
							row![
								text("Times:").width(Length::FillPortion(1)),
								col![p.times[0].view(), p.times[1].view()].width(Length::FillPortion(1))
							].height(Length::FillPortion(1)),
							row![
								text("Board state:").width(Length::FillPortion(1)),
								state
							].height(Length::FillPortion(1)),
							container(button(text("Edit save")).width(Length::Shrink).on_press(Message::Menu(MenuMessage::OpenCloseEdit(true))))
								.width(Length::Fill).align_x(Horizontal::Center).height(Length::FillPortion(1))
						]
						},
						Err(err) => col![text(err).width(Length::Fill).horizontal_alignment(Horizontal::Center).style(Color::new(1., 0., 0., 1.))]
					}
					
				} else { col![] };
				let inner = row![
					container(item_container).padding(10.).width(Length::FillPortion(1)),
					visualize.width(Length::FillPortion(1))
				];
				Modal::new(
					self.is_editing,
					container(col![top, inner]).width(Length::Fill).height(Length::Fill),
					|| {
						let top = row![
							text_input("filename", &*self.selected_filename).width(Length::Fill).on_input(|v| Message::Menu(MenuMessage::EditName(v))).on_submit(Message::Menu(MenuMessage::FinaliseModifyName)),
							button(text("Rename file")).width(Length::Shrink).on_press(Message::Menu(MenuMessage::FinaliseModifyName))
						].spacing(10.);
						let delete = button(text("Delete save")).width(Length::Shrink).on_press(Message::Menu(MenuMessage::DeleteSelected)).style(Button::Destructive);
						let inner = if let Some(err) = &self.edit_input_err {
							col![
								top,
								text(err).width(Length::Fill).style(Color::new(1., 0., 0., 1.)),
								delete
							]
						} else {
							col![top, delete]
						}.spacing(20.).align_items(Alignment::Center).width(Length::Fill);
						Card::new(text("Edit file").width(Length::Fill), inner).width(Length::Fixed(350.)).on_close(Message::Menu(MenuMessage::OpenCloseEdit(false))).into()
					}
				).into()
			}
			MenuState::PlaySettings => self.new_options.view()
		}
	}
}
