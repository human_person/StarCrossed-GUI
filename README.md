# Star Crossed

This repo contains a simple GUI game called Star Crossed. It's like noughts and crosses, but not currently solved and with an element of strategy to it.

How you win is still the same, get a line the size of the board. How you play is different. The boards are made of a grid with diagonal lines:

![example board](img/example_board_dark.png){width=50%}

where you place your markers where the lines meet, not in a square.

You get a limited number of markers to place, after which you move markers around along the lines (one step at a time) until someone wins.

## Acknowledgements

This game was made after seeing it in [this video](https://www.youtube.com/watch?v=ePxrVU4M9uA). So, thanks. It's essentially an extension of the game Three Men's Morris (smallest board size for this game and personally quite dull)
