#!/bin/bash
# Build helper
# This script is to build the project into either a single executable, or a bundle if cargo-bundle is installed
if ! command -v magick &> /dev/null; then
  echo "ImageMagick could not be found and is required"; exit
fi
if ! command -v cargo-bundle &> /dev/null; then
  echo "cargo-bundle not installed. Building without bundling..."
  cargo build --release
else
  if ! command -v magick &> /dev/null; then
    echo "ImageMagick could not be found. Bundle will not have icon"; exit
    cargo bundle --release
  else
    magick convert -background none -resize 512x512 icon.svg icon512x512@2x.png
    magick convert -background none -resize 256x256 icon.svg icon256x256.png
    magick convert -background none -resize 128x128 icon.svg icon128x128.png
    cargo bundle --release
    rm icon512x512@2x.png icon256x256.png icon128x128.png
  fi
fi
